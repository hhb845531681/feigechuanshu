#C语言实现飞鸽传书功能
##一、软件介绍
飞鸽传书是一款局域网通信软件，支持局域网间发信息、文件夹、多文件。可以通过添加局域网外IP来实现网外的聊天与文件传输功能，速度相当快。基于TCP/IP,可运行于多种操作平台，自动搜索在线用户。

我们要做的其实相当于模拟飞鸽传书数据传输间的协议，给相应的客户端、服务端发送相应的数据包达到传输消息/文件的目的。
##二、软件总体框架
![](https://i.imgur.com/A8jDKcT.png)
##三、main函数
![](https://i.imgur.com/oFmENSU.png)
###1.baonum即一个包编号，随机产生，每次发送的数据包包编号都不能相同，是上一个包编号加1。
###2.init_head()，初始化一个用于存上线用户信息的链表的链头，链头为全局变量，这样可以直接拿链头操作，较为方便。
###3.创建线程
####①线程1：自己上线每隔几秒向服务器发送1025包。
####②线程2：创建UDP服务端，绑定2425端口，用于接收数据包。
####③线程3：创建TCP服务端，绑定2425端口，用于传送文件。
###4.循环用于检测输入信息，调用不同的函数进行操作。
##四、UDP网络模块
###1.解包、组包模块
发送消息其实就是通过发送相应格式的数据包来进行比较，现在要进行的是哪个步骤。

我们可以通过wireshark进行抓包，查看不同数据包的格式，一共分为1025广播包、3确认包、33包、487包、288包、96包、2097440包（C语言编写，无界面情况下所用到的包）。
###2.广播流程
登录飞鸽传书即发送1025广播包到服务器表明自己上线。
####①创建UDP套接字，绑定服务器IP-255，端口-2425，设置套接字广播属性。
    int sock = socket(AF_INET,SOCK_DGRAM,0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(2425);
    inet_pton(AF_INET,"192.168.186.255",&addr.sin_addr.s_addr);
    int opt = 1;
    if(setsockopt(sock,SOL_SOCKET,SO_BROADCAST,&opt,sizeof(opt)) != 0)
    {
    perror("setsockopt error");
    return 0;
    }
####②组1025包
通过抓包可以看到1025包的格式如下

![](https://i.imgur.com/JyzVUFp.png)

得到自己的环境、网络配置，写入字符串，组建1025包。

![](https://i.imgur.com/jOTOlAw.png)
####③每隔几秒向服务端发送1025包表示自己在线
###3.上线好友模块
####①通过main函数中线程2接收到1025包即表明有人上线，紧接着通过解析包向发送1025包的用户发送3确认包表明自己已收到上线通知。
####②解析3包
    FEIGE_REQ *jiexibao(char *recvbuf)//传入别人的包，解析包，将上线的用户属性加入链表
    {
    	char *p = NULL;
    	FEIGE_REQ *pnew = NULL;
    	pnew = (FEIGE_REQ *)malloc(sizeof(FEIGE_REQ));
    	if((p = strchr(recvbuf,':')) != NULL)
    	{
    		*p = '\0';
    		strcpy(pnew->version,recvbuf);
    		recvbuf = p + 1;
    	}
    	if((p = strchr(recvbuf,':')) != NULL)
    	{
    		*p = '\0';
    		strcpy(pnew->seq,recvbuf);
    		recvbuf = p + 1;	
    	}
    	if((p = strchr(recvbuf,':')) != NULL)
    	{
    		*p = '\0';
    		strcpy(pnew->user,recvbuf);
    		recvbuf = p + 1;	
    	}
    	if((p = strchr(recvbuf,':')) != NULL)
    	{
    		*p = '\0';
    		strcpy(pnew->host,recvbuf);
    		recvbuf = p + 1;
    	}
    	if((p = strchr(recvbuf,':')) != NULL)
    	{
    		*p = '\0';
    		pnew->cmd = atoi(recvbuf);
    		recvbuf = p + 1;
    	}
    	if((p = strchr(recvbuf,'\0')) != NULL)
    	{
    		strcpy(pnew->name,recvbuf);
    		recvbuf = p + 1;
    	}
    	if((p = strchr(recvbuf,'\0')) != NULL)
    	{
    		strcpy(pnew->group,recvbuf);
    		recvbuf = p + 1;
    	}
    	if((p = strchr(recvbuf,'\0')) != NULL)
    	{
    		strcpy(pnew->mac,recvbuf);
    		recvbuf = p + 2;
    	}
    	if((p = strchr(recvbuf,'\0')) != NULL)
    	{
    		strcpy(pnew->phone,recvbuf);
    		recvbuf = p + 1;
    	}
    	if((p = strchr(recvbuf,'\0')) != NULL)
    	{
    		strcpy(pnew->email,recvbuf);
    		recvbuf = p + 4;
    	}
    	if((p = strchr(recvbuf,'\0')) != NULL)
    	{
    		strcpy(pnew->ip,recvbuf);
    		//printf("%s\n",pnew->ip);
    		recvbuf = p + 2;
    	}
    	if((p = strchr(recvbuf,'\0')) != NULL)
    	{
    		recvbuf = p + 10;
    	}
    	if((p = strchr(recvbuf,'\0')) != NULL)
    	{
    		strcpy(pnew->sign,recvbuf);
    	}
    	pnew->next = NULL;
    	return pnew;
    }
从抓包可以看出数据又':','\0'字符隔开，所以通过strchr函数，拿到有效信息存入用户结构体。
####②组3包
通过抓包可以看到3包的格式如下

![](https://i.imgur.com/yTBUQN5.png)

组3包：

    int packdata_sure(char *buf,int n)//3 确认包
    {
    	int bufsize = 0;
    	int len;
    	memset(buf,0,n);
    	baonum ++;
    	bufsize = sprintf(buf,"%s:%lu:%s:%s:3:",VERSION,baonum,USER,COMPUTER);
    	len = sprintf(buf+bufsize,"%s%c%s%c%s%c%c%s%c%s%c%c5%c",
    		   		  NAME,0,GROUP,0,MAC,0,0,PHONE,0,EMAIL,0,0,0);
    	bufsize += len;
    	len = sprintf(buf+bufsize,"%s%c%c15%c10000001%c%s%c%s",
    			  IP,0,0,0,0,SIGN,0,COMPUTER);
    	bufsize += len;
    	return bufsize;
    }
####③拿到用户信息结构体，遍历链表，通过比较IP加入新用户节点
###4.刷新模块
清空除链头外的节点。
###5.打开聊天窗口模块（主动聊天）
通过输入对方昵称+要发送的聊天信息，遍历链表，得到该昵称对应的IP、Mac，套接字绑定相应IP。
####①发送487包打开聊天窗口
通过抓包可以看到487包的格式如下

![](https://i.imgur.com/ZLpGFit.png)

组487包：

    int data_e7(char *buf,FEIGE_REQ *pnew,int n)//打开聊天窗口，发送487包，写入对方mac地址
    {
    	int size;
    	baonum++;
    	memset(buf,0,n);
    	size = sprintf(buf,"%s:%lu:%s:%s:487:%s",VERSION,baonum,USER,COMPUTER,pnew->mac);
    	return size;
    }
###6.聊天模块
####①组、发送288包，即发送聊天信息
    int data_288(char *buf,char *newsend,int n)//发送聊天信息，发送288包
    {
    	int size;
    	baonum++;
    	memset(buf,0,n);
    	size = sprintf(buf,"%s:%lu:%s:%s:288:%s[rich]0A0000000000860008AE5F6F8FC596D19E12000000000000000000000000000000000000[/rich]",VERSION,baonum,USER,COMPUTER,newsend);
    	return size;
    }
###7.收到数据包后的流程
####①收到2147484026包或487包
应回发33包作出回应，确认自己收到请求。
####②收到288包
即对方给自己发送了聊天信息。首先回发33包表示自己收到，再通过解析288包，打印出聊天内容。
####③收到2097440包（文件传输请求）
#####1）解析2097440包
将解析到的文件信息、对方昵称（通过昵称遍历上线用户链表，得到IP），存入文件的结构体。
#####2）回发96包，表示同意接受文件。通过抓包可以看到96包的格式如下
![](https://i.imgur.com/fuKhrF9.png)
#####3）需注意到96包中最后两个字符串的内容的是收到的2097440包的包编号，以及包编号减1
组96包

    int data_96(char *buf,FEIGE_REQ *pnew,int n)//组96包
    {
    	int size;
    	char seqbuf[20] = {0},seq_1buf[20] = {0};
    	unsigned int seq,seq_1;
    	sscanf(pnew->seq,"%d",&seq);
    	seq_1 = seq-1;
    	sprintf(seqbuf,"%x",seq);
    	sprintf(seq_1buf,"%x",seq_1);
    	baonum++;
    	memset(buf,0,n);
    	size = sprintf(buf,"1.1.180210:%lu:%s:%s:96:%s:%s:0:",baonum,USER,COMPUTER,seqbuf,seq_1buf);
    	return size;
    }
#####4）创建线程，创建客户端，发送96包，接受文件内容
创建TCP客户端套接字，绑定对方IP，端口2425，连接对方服务端，（注意96包通过TCP客户端发送给对方），后创建新文件循环接受信息，写入文件内容。

![](https://i.imgur.com/tDLDzAo.png)
##五、TCP文件传输模块
输入"file"+"对方昵称"+"文件名"（协议可自定），后发送2097440包，表示主动发送文件。
###1.发送2097440包
通过抓包可以看到2097440包的格式如下

![](https://i.imgur.com/9028ccP.png)

组包：根据传入的昵称遍历上线用户链表得到对方IP，通过stat函数得到要传的文件的大小、修改时间等信息。

    int pack_2097440(char *buf,struct stat *st,char *newsend,char *name,int n)//组2097440包
    {
    	memset(buf,0,n);
    	baonum += 2;
    	char atime[20] = {0};
    int size = sprintf(buf, "%s:%lu:%s:%s:%s:%c%lu:%s:%lx:0:1:%lu:%lu:%lu:%lc","5.1.141114",baonum,USER,COMPUTER,"2097440",0,baonum-1,newsend, st->st_size, st->st_ctime, st->st_mtime, st->st_atime, 0);
    	//*sq = baonum+1;
    	/printf("filename:%s\n",newsend);
    	//printf("filesize:%lx\n",st->st_size);
    	//printf("atime:%lu\n",st->st_atime);
    	int i;
    	char baoseq[20] = {0};
    	//char *p = NULL;
    	FEIGE_REQ *pb = head->next;
    	//printf("jinru?:%s\n",buf);
    	sendfilenum++;
    	if(sendfilenum == 10)
    	{
    		perror("too many file\n");
    		return 0;
    	}
    	for(i=0;i<10;i++)
    	{
    		if(sFiles[i].fileid == 0)
    		{
    			sprintf(baoseq,"%lu",baonum);
    			strcpy(sFiles[i].pktid,baoseq);
    			sFiles[i].fileid = sendfilenum;
    			strcpy(sFiles[i].filename,newsend);
    
    			sFiles[i].offset = 0;
    			sFiles[i].filesize = st->st_size;
    			sFiles[i].status = 0;
    			sprintf(atime,"%lu",st->st_atime);
    			strcpy(sFiles[i].starttime,atime);
    			while(pb)
    			{
    				if(strcmp(pb->name,name) == 0)
    				{
    					strcpy(sFiles[i].perrip,pb->ip);
    					//printf("ip:%s\n",pb->ip);
    					break;
    				}
    				pb = pb->next;
    			}
    		}
    		break;
    	}
    
    	return size;
    }
###2.TCP服务端等待96包
main函数中通过创建线程建立TCP服务器，等待客户端的连接，通过解析客户端发送的96包得到文件名，打开相应的文件进行传输。

    void *pthread_file(void *arg)
    {
    	pthread_detach(pthread_self());
    	int ret,i,fp;
    	char pktid[20] = {0};
    	char buf[200] = {0},file[1024] = {0};
    	int clientfd = *(int *)arg;
    	ret = recv(clientfd,buf,sizeof(buf),0);
    	if(ret > 0)
    	{
    		jie_96(pktid,buf,20);
    	}
    	for(i=0;i<10;i++)
    	{	
    		if(strcmp(sFiles[i].pktid,pktid) == 0)
    		{
    			//printf("filename:%s\n",sFiles[i].filename);
    			fp = open(sFiles[i].filename,O_RDWR);
    			if(fp == -1)
    				perror("open file error\n");
    
    			while(read(fp,file,sizeof(file)) > 0)
    			{
    				send(clientfd,file,sizeof(file),0);
    				//printf("send:%s\n",file);
    				memset(file,0,sizeof(file));
    			}
    			close(fp);
    			close(clientfd);
    		}
    		break;
    	}
    	return NULL;
    }