#ifndef _ENVIR_H
#define _ENVIR_H

struct environment  
{
	char name[20];
	char group[20];
	char phone[20];
	char email[25];
	char IP[20];
	char Mac[25];
	//gexing qianming
	struct environment *next;
};
typedef struct environment ENV;


int write_env(ENV *p);//将自己的属性重新写入文件

ENV *read_env();//从文件读取自己的属性

int get_env(char *a);//得到自己的属性

int set_env(char *a);//更改自己的属性

#endif