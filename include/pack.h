#ifndef _PACK_H
#define _PACK_H

struct feigerecv
{
	char version[20];
	char seq[20];
	char user[20];
	char host[20];
	unsigned int cmd;
	char name[200];
	char group[200];
	char mac[25];
	char phone[20];
	char email[25];
	char ip[20];
	char sign[30];
	struct feigerecv *next;
};
typedef struct feigerecv FEIGE_REQ;

int packdata(char *buf,int n);//1025 广播包

int packdata_sure(char *buf,int n);//3 确认包

int data_8e(char *buf,FEIGE_REQ *pnew,int n);//判断是否为8E包，返回33包

int data_e7(char *buf,FEIGE_REQ *pnew,int n);//打开聊天窗口，发送487包，写入对方mac地址

int data_288(char *buf,char *newsend,int n);//发送聊天信息，发送288包

int data_96(char *buf,FEIGE_REQ *pnew,int n);//组96包

int jie_96(char *pktnum,char *buf,int n);//解96包

int pack_2097440(char *buf,struct stat *st,char *newsend,char *name,int n);//组2097440包

FEIGE_REQ *jiexibao(char *recvbuf);//传入别人的上线包，解析包，将上线的用户属性加入链表

int test_ip(FEIGE_REQ *pnew);//检测上线的用户是否已经写入链表，看昵称是否更改

int send_message(char *name,char *newsend);//发送消息

int data_2097440(FEIGE_REQ *pnew);//解析2097400包

void *udp_login(void *arg);//自己上线发送1025 udp 广播包

void *udp_otherlogin(void *arg);//接收别人上线...信息，并做出判断，返回什么包

void *tcp_client(void *arg);//创建客户端，发送96包，接收文件

#endif