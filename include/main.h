#ifndef _MAIN_H
#define _MAIN_H

struct TransFile//用来存文件的信息-----group
{
	char pktid[20];//包编号
	unsigned int fileid;//文件编号
	char filename[20];//文件名
	unsigned int offset;//起始偏移位置
	unsigned int filesize;//文件大小
	int status;//发送状态，0表示未开始，1表示开始
	char starttime[20];//开始发送时间
	char perrip[20];//对方IP
};
struct TransFile rFiles[10];
struct TransFile sFiles[10];


int init_head();//初始化链头，链头保存的属性

int list_user();//展示上线用户的ip、昵称

int free_list();//将链表的用户信息刷掉

int main();

#endif