#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <arpa/inet.h>

#include "envir.h"
#include "pack.h"

#define ENV_FILE "env.txt"

extern unsigned int filenum;
extern unsigned int sendfilenum;
extern long baonum;
extern char VERSION[20];
extern char USER[5];
extern char COMPUTER[20];
extern char NAME[10];
extern char GROUP[15];
extern char PHONE[15];
extern char EMAIL[15];
extern char IP[20];
extern char MAC[20];
extern char SIGN[20];
extern FEIGE_REQ *head;

int write_env(ENV *p)//将自己的属性重新写入文件
{
	FILE *fp;
	fp = fopen(ENV_FILE,"w");
	if(fp == NULL)
	{
		perror("open file error");
		return 0;
	}
	fprintf(fp,"%s\n",p->name);
	fprintf(fp,"%s\n",p->group);
	fprintf(fp,"%s\n",p->phone);
	fprintf(fp,"%s\n",p->email);
	fprintf(fp,"%s\n",p->IP);
	fprintf(fp,"%s\n",p->Mac);
	fclose(fp);
	return 0;
}

ENV *read_env()//从文件读取自己的属性
{
	ENV *head;
	ENV *p,*pold,*pnew;
	FILE *fp;
	fp = fopen(ENV_FILE,"r");
	if(fp == NULL)
		return NULL;
	head = (ENV *)malloc(sizeof(ENV));
	memset(head,0,sizeof(ENV));
	head->next = NULL;
	p = head;
	while(!feof(fp))
	{
		fscanf(fp,"%s",p->name);
		fscanf(fp,"%s",p->group);
		fscanf(fp,"%s",p->phone);
		fscanf(fp,"%s",p->email);
		fscanf(fp,"%s",p->IP);
		fscanf(fp,"%s",p->Mac);
		pnew = (ENV *)malloc(sizeof(ENV));
		memset(pnew,0,sizeof(ENV));
		pnew->next = NULL;
		pold = p;
		p->next = pnew;
		p = pnew;
	}
	memset(p,0,sizeof(ENV));
	free(p);
	pold->next = NULL;
	fclose(fp);
	return head;
}


int get_env(char *a)//得到自己的属性
{
	ENV *head = NULL;
	//char buf[30] = {0};
	//char *p = NULL;

	head = read_env();
	if(strcmp("get name",a) == 0)
	{
		printf("\033[0;33;24mhhb#:\033[0m%s\n",head->name);
	}
	else if(strcmp("get group",a) == 0)
	{
		printf("\033[0;33;24mhhb#:\033[0m%s\n",head->group);
	}
	else if(strcmp("get phone",a) == 0)
	{
		printf("\033[0;33;24mhhb#:\033[0m%s\n",head->phone);
	}
	else if(strcmp("get email",a) == 0)
	{
		printf("\033[0;33;24mhhb#:\033[0m%s\n",head->email);
	}
	else if((strcmp("get IP",a) == 0) || (strcmp("get ip",a) == 0))
	{
		printf("\033[0;33;24mhhb#:\033[0m%s\n",head->IP);
	}
	else if((strcmp("get Mac",a) == 0) || (strcmp("get mac",a) == 0))
	{
		printf("\033[0;33;24mhhb#:\033[0m%s\n",head->Mac);
	}
	return 0;
}

int set_env(char *a)//更改自己的属性
{
	ENV *head = NULL;
	char buf[30] = {0};
	char *p = NULL;

	head = read_env();
	if(strncmp("set name",a,8) == 0)
	{
		memset(buf,0,sizeof(buf));
		//strcpy("name",buf);
		p = strstr(a,"name");
		//strcpy(p,buf);
		strcpy(head->name,p);
		printf("\033[0;33;24mhhb#:\033[0m修改成功\n");
	}
	else if(strncmp("set group",a,9) == 0)
	{
		memset(buf,0,sizeof(buf));
		p = strstr(a,"group");
		strcpy(head->group,p);
		printf("\033[0;33;24mhhb#:\033[0m修改成功\n");
	}
	else if(strncmp("set phone",a,9) == 0)
	{
		memset(buf,0,sizeof(buf));
		p = strstr(a,"phone");
		strcpy(head->phone,p);
		printf("\033[0;33;24mhhb#:\033[0m修改成功\n");
	}
	else if(strncmp("set email",a,9) == 0)
	{
		memset(buf,0,sizeof(buf));
		p = strstr(a,"email");
		strcpy(head->email,p);
		printf("\033[0;33;24mhhb#:\033[0m修改成功\n");
	}
	else if(strncmp("set IP",a,6) == 0 || strncmp("set ip",a,6) == 0)
	{
		/*memset(buf,0,sizeof(buf));
		if(strncmp("set IP",a,6) == 0)
			p = strstr(a,"IP");
		else if(strncmp("set IP",a,6) == 0)
			p = strstr(a,"ip");
	
		strcpy(head->IP,p);*/
		printf("\033[0;33;24mhhb#:\033[0m无法修改\n");
	}
	else if(strncmp("set Mac",a,7) == 0 || strncmp("set mac",a,7) == 0)
	{
		/*memset(buf,0,sizeof(buf));
		if(strncmp("set Mac",a,7) == 0)
			p = strstr(a,"Mac");
		else if(strncmp("set mac",a,7) == 0)
			p = strstr(a,"mac");
		
		strcpy(head->Mac,p);*/
		printf("\033[0;33;24mhhb#:\033[0m无法修改\n");
	}
	write_env(head);
	return 0;
}