#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <arpa/inet.h>

#include "sndfile.h"
#include "pack.h"
#include "main.h"

extern unsigned int filenum;
extern unsigned int sendfilenum;
extern long baonum;
extern char VERSION[20];
extern char USER[5];
extern char COMPUTER[20];
extern char NAME[10];
extern char GROUP[15];
extern char PHONE[15];
extern char EMAIL[15];
extern char IP[20];
extern char MAC[20];
extern char SIGN[20];
extern struct TransFile rFiles[10];
extern struct TransFile sFiles[10];
extern FEIGE_REQ *head;

int createTcpserver()
{
	struct sockaddr_in saddr;
	int sockfd;
	int ret;
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd == -1)
	{
		perror("sock error\n");
		return 0;
	}
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(2425);
	saddr.sin_addr.s_addr = INADDR_ANY;
	ret = bind(sockfd,(struct sockaddr *)&saddr,sizeof(saddr));

	int opt = 1;
	if(setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt)) != 0)
	{
		perror("setsockopt error");
	}

	if(ret == -1)
	{
		perror("bind error\n");
		return 0;
	}
	ret = listen(sockfd,5);
	if(ret == -1)
	{
		perror("listen error\n");
		return 0;
	}
	return sockfd;
}

void *tcp_server(void *arg)
{
	//pthread_detach(pthread_self());
	pthread_t tid;
	int sockfd,clientfd;
	struct sockaddr_in caddr;
	unsigned int len = sizeof(caddr);
	//char buf[200] = {0},file[1024] = {0};
	int ret;
	//char pktid[20] = {0};
	//int fp;

	sockfd = createTcpserver();
	while(1)
	{
		clientfd = accept(sockfd,(struct sockaddr *)&caddr,&len);
		if(clientfd == -1)
		{
			perror("accept error\n");
			return 0;
		}
		int *p = (int *)malloc(sizeof(int));
		*p = clientfd;
		ret = pthread_create(&tid,NULL,pthread_file,p);
		if(ret == -1)
		{
			perror("create pthread error\n");
		}
		/*
		ret = recv(clientfd,buf,sizeof(buf),0);
		//printf("recv[%d]:%s\n",ret,buf);
		//getchar();
		if(ret > 0)
		{
			jie_96(pktid,buf);
		}
		for(i=0;i<10;i++)
		{	
			if(strcmp(sFiles[i].pktid,pktid) == 0)
			{
				//printf("filename:%s\n",sFiles[i].filename);
				fp = open(sFiles[i].filename,O_RDWR);
				if(fp == -1)
					perror("open file error\n");

				while(read(fp,file,sizeof(file)) > 0)
				{
					send(clientfd,file,sizeof(file),0);
					//printf("send:%s\n",file);
					memset(file,0,sizeof(file));
				}
				close(fp);
				close(clientfd);
			}
			break;
		}
		*/
	}
	close(sockfd);
	return NULL;
}


void *pthread_file(void *arg)
{
	pthread_detach(pthread_self());
	int ret,i,fp;
	char pktid[20] = {0};
	char buf[200] = {0},file[1024] = {0};
	int clientfd = *(int *)arg;
	ret = recv(clientfd,buf,sizeof(buf),0);
	//printf("recv[%d]:%s\n",ret,buf);
	//getchar();
	if(ret > 0)
	{
		jie_96(pktid,buf,20);
	}
	for(i=0;i<10;i++)
	{	
		if(strcmp(sFiles[i].pktid,pktid) == 0)
		{
			//printf("filename:%s\n",sFiles[i].filename);
			fp = open(sFiles[i].filename,O_RDWR);
			if(fp == -1)
				perror("open file error\n");

			while(read(fp,file,sizeof(file)) > 0)
			{
				send(clientfd,file,sizeof(file),0);
				//printf("send:%s\n",file);
				memset(file,0,sizeof(file));
			}
			close(fp);
			close(clientfd);
		}
		break;
	}
	return NULL;
}

int send_file(char *name,char *newsend) //发送2097440包
{
	struct stat st;
	stat(newsend,&st);
	int sock = socket(AF_INET,SOCK_DGRAM,0);
	int opt = 1;
	if(setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt)))
	{
		perror("setsockopt error\n");
	}
	struct sockaddr_in send_addr;
	int send_len = sizeof(send_addr);
	char buf[1024] = {0};
	int size;
	FEIGE_REQ *new = NULL;
	new = head->next;
	while(new)
	{
		if(!strcmp(new->name,name))
			break;
		new = new->next;
	}
	if(new)
	{
		send_addr.sin_family = AF_INET;
		send_addr.sin_port = htons(2425);
		inet_pton(AF_INET,new->ip,&send_addr.sin_addr.s_addr);
		size = pack_2097440(buf,&st,newsend,name,1024);
		//printf("2097440:%s\n",buf);
		sendto(sock,buf,size,0,(struct sockaddr *)&send_addr,send_len);
	}
	return 0;
} 